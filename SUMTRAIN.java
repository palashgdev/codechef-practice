import java.util.Scanner;
import java.util.Arrays;

class SUMTRAIN{
  public static void main(String[] args) {
    Scanner kb=new Scanner(System.in);
    int test = kb.nextInt();
    int ans[]=new int [test];
    for(int i=0;i<test;i++){
      int n=kb.nextInt();
      int sum=0;
      for(int j=0;j<n;j++){
        int array[] = new int[j+1];
        for(int k=0;k<array.length;k++){
          array[k]=kb.nextInt();
        }
        Arrays.sort(array);
        if(j>0){
          sum=sum+array[array.length-1];
        }
    }
    ans[i]=sum;
  }
  for(int p=0;p<ans.length;p++){
    System.out.println(ans[p]);
  }
}
}