import java.util.*;
class FLOW001{
  public static void main(String[] args) {
    Scanner kb=new Scanner(System.in);
    int test=kb.nextInt();
    int a[][] = new int[test][2];
    int ans[] = new int[test];
    for(int i=0;i<test;i++){
      for(int j=0;j<2;j++){
        a[i][j] = kb.nextInt();
      }
    }

    for(int i=0;i<test;i++){
      ans[i] = a[i][0]+a[i][1];
    }

    for(int k=0;k<ans.length;k++){
      System.out.println(ans[k]);
    }
  }
}